
Overview
---------

The PAM Auth module is based vaguely on a skeleton of the IMAP Auth module
(http://drupal.org/project/imap_auth).  It accomplishes the same goal except
that it uses the PHP5-PAM module to accomplish the authentication.  It also 
borrows some ideas from the ldap_integration module for password-form
adjustments.

Requirements
------------
* Drupal 5.x or later
* Php PAM extension
  [http://packages.debian.org/etch/php5-auth-pam]

Install
-------

1. Copy the 'pam_auth' module directory in to your Drupal
   modules directory as usual

2. Activate the module via the Drupal module configuration menu
   (Administer >> Site building >> Modules)

3. Configuration: "Administer >> Site configuration >> PAM auth"

PAM authentication:
 * Enabled: Users will be able to get logged in using PAM accounts
 * Disabled : Turn off PAM authentication.

Password changing forms:
 * Enabled: Users will still be able to change their passwords on edit user
pages and request new passwords from the login prompts.
 * Disabled: Password forms will be altered so that users cannot change
their passwords.  This is recommended because this module does not relay
those changes to the underlying PAM authentication.

Important Note for when it doesn't work!!
-----------------------------------------
The PHP PAM module is not particularly popular because it requires the PHP
process owner to be privileged enough to perform the PAM authentication.  If
you're using /etc/shadow for your usernames and passwords, that probably means
that the Apache process owner needs to be added to the shadow group.  Every
distro may be different and different PAM configurations (like LDAP) may be
more or less possible.  But in the common case of using shadow passwords as
most Linux systems do by default, Apache will need to be able to read
/etc/shadow.  Please just be aware of the potential security implications of
doing that, however obscure.

Authors
-------
Neil Schelly     [http://drupal.org/user/182824]

License
-------
GPL (see LICENSE.txt)
